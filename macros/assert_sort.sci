// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2008 - ENPC - Bruno Pincon
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function [ x , indices ] = assert_sort ( varargin )
  //   A flexible sorting function.
  //
  // Calling Sequence
  //   y = assert_sort ( x )
  //   y = assert_sort ( x , direction )
  //   y = assert_sort ( x , direction , compfun )
  //   y = assert_sort ( x , direction , compfun , callback )
  //   [ y , indices ]  = assert_sort ( ... )
  //
  // Parameters
  //   x : a matrix of doubles or a list, the values to sort.
  //   direction : a 1x1 matrix of booleans, the direction of sort. If direction=%t, increasing sorting is used. If direction=%f, decreasing sorting is used. (Default direction=%t, i.e., increasing). If direction=[], then default direction=%t is used.
  //   compfun : a function or a list, the comparison function. If not provided, or if compfun=[], then the default value compfun=assert_compare is used.
  //   callback : a function, an output function. If not provided, or if callback=[], then the default value callback=[] is used, meaning that no function is called back.
  //   y : a matrix of doubles or a list, the sorted values
  //   indices : a matrix of floating point integers, the permutation necessary to get the sorted values. We always have and(x(indices)==y).
  //
  // Description
  //   Sort a matrix or a list in increasing order.
  //
  //   If x is a list, the user must provide a comparison function, as 
  //   no default comparison function is available for this data type.
  //
  //   If no comparison function is given, 
  //   uses the default operator "<".
  //   If compfun is a function, it must have the header :
  //   <programlisting>
  //   order = compfun ( x , y )
  //   </programlisting>
  //   where order=-1 if x < y, order=0 if x==y, order +1 if x > y.
  //
  //   If compfun is the list (myfun,a1,a2,...), its first element myfun is expected to 
  //   be a function which must have the header:
  //   <programlisting>
  //   order = myfun ( x , y , a1, a2, ... )
  //   </programlisting>
  //   where the remaining elements a1, a2, ... are automatically added 
  //   to the calling sequence.
  //
  //   If callback is a function, it must have the header :
  //   <programlisting>
  //   callback ( status , x , imin , imax )
  //   </programlisting>
  //   where status is a string, x is the current data, imin and imax are
  //   floating point integers.
  //   The values of status are status="start","in","out","stop".
  //   The "start" status appears only once, at the very begining.
  //   The "stop" status appears only once, at the very end.
  //   The "in" status appears when we enter in the recursive function.
  //   The "out" status appears when we leave the recursive function.
  //   The imin and imax variables contain the global indices
  //   which are currently modified in the recursive call.
  //   These indices correspond to the values which are to be
  //   modified in the level 0 array to be sorted, as
  //   opposed to the array currently being processed at the
  //   current  level.
  //   These indices are necessary, since the function basically
  //   know only the current values to be processed, which do not
  //   reflect their original positions.
  //
  //   If callback is the list (mycbk,a1,a2,...), its first element mycbk is expected to 
  //   be a function which must have the header:
  //   <programlisting>
  //   mycbk ( status , x , imin , imax , a1, a2, ... )
  //   </programlisting>
  //   where the remaining elements a1, a2, ... are automatically added 
  //   to the calling sequence.
  //
  //   The algorithm is a merge-sort, based on a recursive 
  //   call of the function, with increasingly small data.
  //
  // This implementation avoids the overhead caused by arguments checking.
  // For that purpose, the argument checking is done at the main level once for all.
  // Then it uses an internal recursive function without
  // any checking.
  //
  // TODO : add a "unique" option, which discards equal elements.
  //
  // Examples
  //   // Sort x in increasing order
  //   x = [4 5 1 2 3]
  //   y = assert_sort ( x )
  //
  //   // Get the indices
  //   x = [4 5 1 2 3]
  //   [y,indices] = assert_sort ( x )
  //   x(indices)==y
  //
  // // Sort x in decreasing order
  // x = [4 5 1 2 3];
  // y = assert_sort ( x , %f )
  //
  //   // Use a customized comparison function:
  //   // sort into decreasing order.
  // function order = myorder ( x , y )
  //   if ( x < y ) then
  //     order = 1
  //   elseif ( x==y ) then
  //     order = 0
  //   else 
  //     order = -1
  //   end
  // endfunction
  //   x = [4 5 1 2 3]
  //   // Notice that we use the default direction (i.e. increasing)
  //   // by setting direction to the empty matrix [].
  //   y = assert_sort ( x , [] , myorder )
  //
  //   // Use a customized comparison function:
  //   // sort real values into increasing order, 
  //   // with an absolute tolerance.
  // function order = myrealorder ( x , y , atol )
  //   if ( abs(x-y)<atol ) then
  //     order = 0
  //   elseif ( x < y ) then
  //     order = -1
  //   else 
  //     order = 1
  //   end
  // endfunction
  //   x = [1,2,1.2347,1.2346,1.2345,3,4]
  //   atol = 1.e-3
  //   compfun = list(myrealorder,atol)
  //   y = assert_sort ( x , [] , compfun )
  //   // Notice that the basic comparison function would 
  //   // produce a difference sorted matrix.
  //   assert_sort ( x )
  //   // See that this is a stable sort.
  //   // Notice that the equal values are not moved.
  //   y = assert_sort ( x , %f , compfun )
  //
  // // Check that we can sort lists.
  // // We consider list of elements, where each element
  // // is a couple. The order depends on the first element (denoted "x"),
  // // then the second element (denoted "y"), in case of tie.
  // z = list();
  // z(1) = list(6,5);
  // z(2) = list(5,4);
  // z(3) = list(4,3);
  // z(4) = list(3,2);
  // z(5) = list(2,1);
  // z(6) = list(1,0);
  // sci2exp(zsorted)
  // // Use a customized comparison function:
  // // sort into increasing order.
  // function order = myorder2 ( a , b )
  //   ax = a(1)
  //   ay = a(2)
  //   bx = b(1)
  //   by = b(2)
  //   if ( ax == bx ) then
  //     if ( ay == by ) then
  //       order = 0
  //     elseif ( ay < by ) then
  //       order = -1
  //     else
  //       order = 1
  //     end
  //   elseif ( ax < bx ) then
  //     order = -1
  //   else
  //     order = 1
  //   end
  // endfunction
  // zsorted = assert_sort(z,[],myorder2);
  // sci2exp(zsorted)
  //
  // // Use a callback to print the status of sorting
  // function mycb ( status , x , imin , imax )
  //   mprintf("%s: (%d,%d) : %s\n",status,imin,imax,sci2exp(x))
  // endfunction
  // // Sort this into [1 2 3 4 5]
  // x = [4 5 1 2 3];
  // // Notice that we set compfun = [], so that the default 
  // // comparison function is used.
  // y = assert_sort ( x , [] , [] , mycb );
  //
  // // Display a graphical demo of the sorting process
  // function mycb2 ( status , x , imin , imax )
  //   n = length(x)
  //   if ( status == "start") then
  //     h = scf()
  //     xp = 1:n
  //     yp = x
  //     plot(xp,yp,"b*")
  //     titlestr = "Merge sort n="+string(n)+" indices = ("+string(imin)+","+string(imax)+")"
  //     xtitle(titlestr,"Rank","Value")
  //   elseif ( status == "out") then
  //     h = gcf()
  //     h.children.children.children.data(imin:imax,2) = x
  //     titlestr = "Merge sort n="+string(n)+" indices = ("+string(imin)+","+string(imax)+")"
  //     h.children.title.text = titlestr
  //   end
  // endfunction
  // N = 50;
  // // Take the values 1,2,...,50 and permute them randomly
  // x = (1:50)';
  // x = grand(1,"prm",x);
  // y = assert_sort ( x , [] , [] , mycb2 );
  //
  // Authors
  // Bruno Pincon, 2008, "Quelques tests de rapidite entre differents logiciels matriciels"
  // Michael Baudin, DIGITEO, 2009-2010 (to manage a comparison function)
  //
  // Bibliography
  // http://gitweb.scilab.org/?p=scilab.git;a=blob_plain;f=scilab/modules/cacsd/tests/nonreg_tests/bug_68.tst;h=920d091d089b61bf961ea9e888b4d7d469942a14;hb=4ce3d4109dd752fce5f763be71ea639e09a12630
  //

  [lhs,rhs]=argn()
  if ( and( rhs<>[1 2 3 4] ) ) then
    errmsg = sprintf("%s: Unexpected number of arguments : %d provided while %d to %d are expected.","assert_sort",rhs,1,4);
    error(errmsg)
  end
  // Get the array x
  x = varargin(1)
  direction = argindefault ( rhs , varargin , 2 , %t )
  __assertsort_compfun__ = argindefault ( rhs , varargin , 3 , assert_compare )
  __assertsort_callback__ = argindefault ( rhs , varargin , 4 , [] )
  //
  // Check types
  if ( and(typeof(x)<>["constant" "list"]) ) then
    errmsg = sprintf("%s: Unexpected type for input argument #%d: variable %s is %s while constant or list is expected.","assert_sort",1,"x",typeof(x));
    error(errmsg)
  end
  if ( and(typeof(direction)<>"boolean") ) then
    errmsg = sprintf("%s: Unexpected type for input argument #%d: variable %s is %s while %s is expected.","assert_sort",2,"direction",typeof(direction),"boolean");
    error(errmsg)
  end
  if ( and(typeof(__assertsort_compfun__)<>["function" "list"] ) ) then
    errmsg = sprintf("%s: Unexpected type for input argument #%d: variable %s is %s while function or list is expected.","assert_sort",3,"compfun",typeof(__assertsort_compfun__));
    error(errmsg)
  end
  if ( __assertsort_callback__ <> [] ) then
    if ( and(typeof(__assertsort_callback__)<>["function" "list"]) ) then
      errmsg = sprintf("%s: Unexpected type for input argument #%d: variable %s is %s while function or list is expected.","assert_sort",4,"callback",typeof(__assertsort_callback__));
      error(errmsg)
    end
  end
  if ( typeof(x)=="list" & __assertsort_compfun__==[] ) then
    errmsg = sprintf("%s: The variable %s is a list, but no comparison function %s is provided.","assert_sort","x","compfun");
    error(errmsg)
  end
  //
  // Check sizes
  if ( size(direction)<>[1 1] ) then
    errmsg = sprintf("%s: Unexpected size for input argument #%d: variable %s has size [%d %d] while scalar is expected.","assert_sort",2,"direction",size(x,"r"),size(x,"c"));
    error(errmsg)
  end
  // 
  // Proceed recursively
  typx = typeof(x)
  if ( typx == "constant" ) then
    nrows = size(x,"r")
    ncols = size(x,"c")
  end
  imin = 1
  imax = length(x)
  if ( __assertsort_callback__ <> [] ) then
    tcallback = typeof(__assertsort_callback__)
    if ( tcallback=="list" ) then
      __assertsort_cbck__f_ = __assertsort_callback__ ( 1 )
      __assertsort_cbck__f_ ( "start" , x , imin , imax , __assertsort_callback__(2:$) )
    else
      __assertsort_callback__ ( "start" , x , imin , imax )
    end
  end
  [ x , indices ] = assert_sortbisre ( typx , x , direction , __assertsort_compfun__ , __assertsort_callback__ , imin , imax )
  if ( __assertsort_callback__ <> [] ) then
    if ( tcallback=="list" ) then
      __assertsort_cbck__f_ ( "stop" , x , imin , imax , __assertsort_callback__(2:$) )
    else
      __assertsort_callback__ ( "stop" , x , imin , imax )
    end
  end
  // Reformat the matrix
  if ( typx == "constant" ) then
    x = matrix(x,nrows,ncols)
  end
endfunction
function [ x , indices ] = assert_sortbisre ( typx , x , direction , __assertsort_compfun__ , __assertsort_callback__ , imin , imax )
  // Immediately Proceed.
  // We assume that the arguments are correct.
  if ( __assertsort_callback__ <> [] ) then
    tcallback = typeof(__assertsort_callback__)
    if ( tcallback=="list" ) then
      __assertsort_cbck__f_ = __assertsort_callback__ ( 1 )
      __assertsort_cbck__f_ ( "in" , x , imin , imax , __assertsort_callback__(2:$) )
    else
      __assertsort_callback__ ( "in" , x , imin , imax )
    end
  end
  n = length(x)
  indices = 1 : n
  if ( n > 1 ) then
    m = floor(n/2)
    p = n-m
    if ( typx=="constant" ) then
      sx1 = x(1:m)
      sx2 = x(m+1:n)
    else
      sx1 = list(x(1:m))
      sx2 = list(x(m+1:n))
    end
    [x1,indices1] = assert_sortbisre ( typx , sx1 , direction , __assertsort_compfun__ , __assertsort_callback__ , imin , imin+m-1 )
    [x2,indices2] = assert_sortbisre ( typx , sx2 , direction , __assertsort_compfun__ , __assertsort_callback__ , imin+m , imax )
    // Merge the two lists x1 and x2 into x.
    [ x , indices ] = merge ( typx , x1 , x2 , indices1 , indices2 , direction , __assertsort_compfun__ , imin , imax )
  end
  if ( __assertsort_callback__ <> [] ) then
    if ( tcallback=="list" ) then
      __assertsort_cbck__f_ ( "out" , x , imin , imax , __assertsort_callback__(2:$) )
    else
      __assertsort_callback__ ( "out" , x , imin , imax )
    end
  end
endfunction
function [ x , indices ] = merge ( typx , x1 , x2 , indices1 , indices2 , direction , __assertsort_compfun__ , imin , imax )
  // Merge the two lists x1 and x2 into x.
  n1 = length(x1)
  n2 = length(x2)
  n = n1 + n2
  
  indices = 1 : n
  if ( typx=="constant" ) then
    x = []
  else
    x = list()
  end
  
    i = 1 
    i1 = 1
    i2 = 1
    if ( typeof(__assertsort_compfun__)=="list" ) then
      __assertsort_compf__f_ = __assertsort_compfun__(1)
    end
    for i = 1:n
      if ( typeof(__assertsort_compfun__)=="list" ) then
        order = __assertsort_compf__f_ ( x1(i1) , x2(i2) , __assertsort_compfun__(2:$) )
      else
        order = __assertsort_compfun__ ( x1(i1) , x2(i2) )
      end
      if ( ~direction ) then
        order = -order
      end
      if ( order<=0 ) then
        indices(i) = indices1(i1)
        x(i) = x1(i1)
        i1 = i1+1
        if (i1 > m) then
          indices(i+1:n) = indices2(i2:p) + n1
          // Apply to x
          if ( typx=="constant" ) then
            x(i+1:n) = x2(i2:p)
          else
            k = i2
            for j = i+1 : n
              x(j) = x2(k)
              k = k + 1
            end
          end
          break
        end
      else
        indices(i) = indices2(i2) + n1
        x(i) = x2(i2)
        i2 = i2+1
        if (i2 > p) then
          indices(i+1:n) = indices1(i1:m)
          // Apply to x
          if ( typx=="constant" ) then
            x(i+1:n) = x1(i1:m)
          else
            k = i1
            for j = i+1 : n
              x(j) = x1(k)
              k = k + 1
            end
          end
          break
        end
      end
    end
endfunction


// Returns the value of the input argument #arg
function argin = argindefault ( rhs , vararglist , ivar , default )
  if ( rhs < ivar ) then
    argin = default
  else
    if ( vararglist(ivar) <> [] ) then
      argin = vararglist(ivar)
    else
      argin = default
    end
  end
endfunction



