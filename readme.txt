Assert toolbox

Purpose
----

The goal of this toolbox is to provide functions to make testing easier. 
We emphasize the use of consistent tools for testing numerical issues,
with the goal of testing numerical algorithms more easily.
In particular, we provide a comparison function for two floating
point numbers, which allows to check that two numbers are "numerically almost equal",
i.e. that the relative error is small.

The function of the assert module are designed to be used in Scilab unit tests files (.tst files).

The functions allows to:
 * assert for true, equal statements,
 * assert that two floating point numbers are "close" (using a relative or absolute tolerance).

Additionnaly, we can sort doubles complex numbers with a mixed relative/absolute tolerance criteria.

The toolbox is based on macros.

To get an overview of the functions in this module, 
use :

help assert_overview


Features
--------

The following is a list of the current assert functions :
 * assert_checkalmostequal : Check that computed and expected are numerically close.
 * assert_checkequal : Check that computed and expected are equal.
 * assert_checktrue : Check that condition is true.
 * assert_checkfalse : Check that condition is false.
 * assert_checkfilesequal : Check two files are equal.
 * assert_checkerror : Check that the function has produced the correct error.
 * assert_generror : Generates an error.

and sorting functions :
 * assert_compare : The default comparison function used in the sort-merge.
 * assert_comparecomplex : Compare complex numbers with a tolerance.
 * assert_sort : A flexible sorting function.
 * assert_sortcomplex : Compare complex numbers with a tolerance.

and support functions :
 * assert_computedigits : Returns the number of significant digits in computed result.
 * assert_cond2reltol : Suggests a relative error, computed from the condition number.
 * assert_cond2reqdigits : Suggests the number of required digits, given the condition number.
 * assert_csvread : Read data from a file or a string.
 * assert_csvwrite : Write data into a file or a string.

TODO
----

 * create assert_computerelerr with management for nan, inf
 * create assert_checkdigits
 * create assert_condmaxsearch, based on a combination of fminsearch and assert_condnum
 * assert_computedigits : use inside a assert_digits ( computed , expected , dmin ) function
 * assert_computedigits : add a comptype option to make the comparison "matrix" (current is "element")
 * assert_sort : add a "unique" option, which discards equal elements.
 * assert_checkequal : remove the mythrownan function when the bug fix in thrownan is integrated in Scilab release.
 * assert_csvwrite : allow A to be a matrix of strings to format

Author
------

Copyright (C) 2008 - 2009 - INRIA - Michael Baudin
Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin

Forge
-----

http://forge.scilab.org/index.php/p/assert/

ATOMS
-----

http://atoms.scilab.org/add/assert

Licence
----

This toolbox is released under the CeCILL_V2 licence :

http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

