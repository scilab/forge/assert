// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->

function flag = MY_assert_equal ( computed , expected )
    if computed(find(~isnan(computed))) == expected(find(~isnan(expected)))  then
        flag = 1;
        if find(isnan(computed))==find(isnan(expected))  then
            flag = 1
        else
            flag = 0
        end
    else
        flag = 0;
    end
    if flag <> 1 then pause,end
endfunction

Astr = [
"1" "8" "15" "22" "29" "36" "43" "50"
"2" "9" "16" "23" "30" "37" "44" "51"
"3" "10" "17" "6+3*I" "31" "38" "45" "52"
"4" "11" "18" "25" "32" "39" "46" "53"
"5" "12" "19" "26" "33" "40" "47" "54"
"6" "13" "20" "27" "34" "41" "48" "55"
"+0" "-0" "Inf" "-Inf" "Nan" "1.D+308" "1.e-308" "1.e-323"
];

// Create a file with some data separated with commas    
filename = fullfile(TMPDIR , "foo.csv");
sep = ",";
fd = mopen(filename,'wt');
for i = 1 : size(Astr,"r")
    mfprintf(fd,"%s\n",strcat(Astr(i,:),sep));
end
mclose(fd);
//   edit(filename);
// Read this file
Bstr = assert_csvread ( filename );
expected = [
"1" "8" "15" "22" "29" "36" "43" "50"
"2" "9" "16" "23" "30" "37" "44" "51"
"3" "10" "17" "6+3*%i" "31" "38" "45" "52"
"4" "11" "18" "25" "32" "39" "46" "53"
"5" "12" "19" "26" "33" "40" "47" "54"
"6" "13" "20" "27" "34" "41" "48" "55"
"+0" "-0" "%inf" "-%inf" "%nan" "1.D+308" "1.e-308" "1.e-323"
];
MY_assert_equal ( Bstr , expected );
// Analyze the file
Bdbl = evstr(Bstr);
Adbl = [
1 8 15 22 29 36 43 50
2 9 16 23 30 37 44 51
3 10 17 6+3*%i 31 38 45 52
4 11 18 25 32 39 46 53
5 12 19 26 33 40 47 54
6 13 20 27 34 41 48 55
+0 -0 %inf -%inf %nan 1.D+308 1.e-308 1.e-323
];
MY_assert_equal ( Adbl , Bdbl );
/////////////////////////////////////////////////////
//
// Read this file with a comma as separator
Bstr = assert_csvread ( filename , "," );
MY_assert_equal ( Bstr , expected );
/////////////////////////////////////////////////////
//
// Read a file with customized separator
filename = fullfile(TMPDIR , "foo.csv");
sep = ";";
fd = mopen(filename,'wt');
for i = 1 : size(Astr,"r")
    mfprintf(fd,"%s\n",strcat(Astr(i,:),sep));
end
mclose(fd);
Bstr = assert_csvread ( filename , sep );
MY_assert_equal ( Bstr , expected );
/////////////////////////////////////////////////////
//
// Read a file ignoring the comments
filename = fullfile(TMPDIR , "foo.csv");
sep = ",";
fd = mopen(filename,'wt');
mfprintf(fd,"#This is a comment\n");
for i = 1 : size(Astr,"r")
    mfprintf(fd,"%s\n",strcat(Astr(i,:),sep));
    mfprintf(fd,"# This is another comment\n");
end
mfprintf(fd,"# This is a final comment\n");
mclose(fd);
Bstr = assert_csvread ( filename );
MY_assert_equal ( Bstr , expected );
/////////////////////////////////////////////////////
//
// Read a file ignoring customized comments
filename = fullfile(TMPDIR , "foo.csv");
sep = ",";
fd = mopen(filename,'wt');
mfprintf(fd,"C This is a comment\n");
for i = 1 : size(Astr,"r")
    mfprintf(fd,"%s\n",strcat(Astr(i,:),sep));
    mfprintf(fd,"C This is another comment\n");
end
mfprintf(fd,"C This is a final comment\n");
mclose(fd);
Bstr = assert_csvread ( filename , [] , [] , "/C(.*)/" );
MY_assert_equal ( Bstr , expected );
/////////////////////////////////////////////////////
//
// Read a string instead of a file
Atext = "";
sep = ",";
for i = 1 : size(Astr,"r")
    Atext(i) = msprintf("%s\n",strcat(Astr(i,:),sep));
end
// Read this data directly
Bstr = assert_csvread ( Atext , [] , [] , [] , %t );
MY_assert_equal ( Bstr , expected );
/////////////////////////////////////////////////////
//
// Read a small data set
Atext = "1";
Bstr = assert_csvread ( Atext , [] , [] , [] , %t );
MY_assert_equal ( Bstr , "1" );
/////////////////////////////////////////////////////
//
// Read a data file with particular Infinities and complex numbers
Astr = [
"6+3*I" "13-7*I" "20+4*I" "27-1.5*I" "34+3.14*I" "41-3*I" "48+3*I" "55-7*I"
"+0" "-0" "Infinity" "-Infinity" "Nan" "1.D+308" "1.e-308" "1.e-323"
];
expected = [
"6+3*%i" "13-7*%i" "20+4*%i" "27-1.5*%i" "34+3.14*%i" "41-3*%i" "48+3*%i" "55-7*%i"
"+0" "-0" "%inf" "-%inf" "%nan" "1.D+308" "1.e-308" "1.e-323"
];
filename = fullfile(TMPDIR , "foo.csv");
sep = ",";
fd = mopen(filename,'wt');
for i = 1 : size(Astr,"r")
    mfprintf(fd,"%s\n",strcat(Astr(i,:),sep));
end
mclose(fd);
replacemap=["Nan","%nan";"Infinity","%inf";"I","%i"];
Bstr = assert_csvread ( filename , [] , [] , [] , [] , replacemap );
MY_assert_equal ( Bstr , expected );
Bdbl = evstr(Bstr);
Adbl = [
6+3*%i 13-7*%i 20+4*%i 27-1.5*%i 34+3.14*%i 41-3*%i 48+3*%i 55-7*%i
+0 -0 %inf -%inf %nan 1.D+308 1.e-308 1.e-323
];
MY_assert_equal ( Adbl , Bdbl );
/////////////////////////////////////////////////////
//
// Configure the decimal mark.
Atext = [
" 1,000000000D+00; 0,000000000D+00; 2,000000000D+02;             Inf; 0,000000000D+00";  
" 1,000000000D+00; 1,00000000D-300; 2,000000000D+02;             Inf; 0,000000000D+00";   
" 1,000000000D+00; 1,00000000D-200; 2,000000000D+02; 3,15000000D+300; 1,020000000D+02";   
" 9,999999999D-01; 1,00000000D-100; 2,000000000D+02; 2,960000000D+02; 1,170000000D+02";   
" 1,000000000D+00;             Inf;            -Inf;             Nan; 0,000000000D+00"
];
Bstr = assert_csvread ( Atext , ";" , "," , [] , %t );
expected = [
"1.000000000D+00" "0.000000000D+00" "2.000000000D+02"            "%inf" "0.000000000D+00" 
"1.000000000D+00" "1.00000000D-300" "2.000000000D+02"            "%inf" "0.000000000D+00"  
"1.000000000D+00" "1.00000000D-200" "2.000000000D+02" "3.15000000D+300" "1.020000000D+02"  
"9.999999999D-01" "1.00000000D-100" "2.000000000D+02" "2.960000000D+02" "1.170000000D+02"  
"1.000000000D+00"            "%inf"           "-%inf"            "%nan" "0.000000000D+00"
];
MY_assert_equal ( Bstr , expected );
/////////////////////////////////////////////////////
//
// Check that datasetread and datasetwrite are inverse.
// Directly access to the string.
// A = rand(3,4)+rand(3,4)*%i
A = [
  5.874312063679099083D-02+%i*8.843077751807868481D-01,..
  7.757596764713525772D-02+%i*1.152209592983126640D-01,..
  5.172297963872551918D-02+%i*8.805298106744885445D-02,..
  4.900220278650522232D-01+%i*2.017885632812976837D-01;
  8.258464913815259933D-01+%i*7.191293761134147644D-01,..
  5.846092323772609234D-01+%i*4.862680672667920589D-01,..
  5.958625068888068199D-01+%i*7.008561277762055397D-01,..
  5.272795078344643116D-01+%i*4.062821255065500736D-01;
  2.980741565115749836D-01+%i*6.942595774307847023D-02,..
  7.528713606297969818D-01+%i*7.671582605689764023D-01,..
  3.833705312572419643D-01+%i*1.879138792864978313D-01,..
  6.889454741030931473D-02+%i*4.096656953915953636D-01
];
Astr = assert_csvwrite(A);
Bstr = assert_csvread(Astr,[],[],[],%t);
B = evstr(Bstr);
MY_assert_equal ( and(B==A) , %t );
/////////////////////////////////////////////////////
//
// Check that datasetread and datasetwrite are inverse.
// Configure the filename.
A = [
  5.874312063679099083D-02+%i*8.843077751807868481D-01,..
  7.757596764713525772D-02+%i*1.152209592983126640D-01,..
  5.172297963872551918D-02+%i*8.805298106744885445D-02,..
  4.900220278650522232D-01+%i*2.017885632812976837D-01;
  8.258464913815259933D-01+%i*7.191293761134147644D-01,..
  5.846092323772609234D-01+%i*4.862680672667920589D-01,..
  5.958625068888068199D-01+%i*7.008561277762055397D-01,..
  5.272795078344643116D-01+%i*4.062821255065500736D-01;
  2.980741565115749836D-01+%i*6.942595774307847023D-02,..
  7.528713606297969818D-01+%i*7.671582605689764023D-01,..
  3.833705312572419643D-01+%i*1.879138792864978313D-01,..
  6.889454741030931473D-02+%i*4.096656953915953636D-01
];
filename = fullfile(TMPDIR,"foo.csv");
assert_csvwrite(A,filename);
Bstr = assert_csvread(filename);
B = evstr(Bstr);
MY_assert_equal ( and(B==A) , %t );
/////////////////////////////////////////////////////
//
// Check that datasetread and datasetwrite are inverse.
// Configure the separator.
A = [
  5.874312063679099083D-02+%i*8.843077751807868481D-01,..
  7.757596764713525772D-02+%i*1.152209592983126640D-01,..
  5.172297963872551918D-02+%i*8.805298106744885445D-02,..
  4.900220278650522232D-01+%i*2.017885632812976837D-01;
  8.258464913815259933D-01+%i*7.191293761134147644D-01,..
  5.846092323772609234D-01+%i*4.862680672667920589D-01,..
  5.958625068888068199D-01+%i*7.008561277762055397D-01,..
  5.272795078344643116D-01+%i*4.062821255065500736D-01;
  2.980741565115749836D-01+%i*6.942595774307847023D-02,..
  7.528713606297969818D-01+%i*7.671582605689764023D-01,..
  3.833705312572419643D-01+%i*1.879138792864978313D-01,..
  6.889454741030931473D-02+%i*4.096656953915953636D-01
];
Astr = assert_csvwrite(A,[],";");
Bstr = assert_csvread(Astr,";",[],[],%t);
B = evstr(Bstr);
MY_assert_equal ( and(B==A) , %t );
/////////////////////////////////////////////////////
//
// Check that datasetread and datasetwrite are inverse.
// Configure the decimal mark.
A = [
  5.874312063679099083D-02+%i*8.843077751807868481D-01,..
  7.757596764713525772D-02+%i*1.152209592983126640D-01,..
  5.172297963872551918D-02+%i*8.805298106744885445D-02,..
  4.900220278650522232D-01+%i*2.017885632812976837D-01;
  8.258464913815259933D-01+%i*7.191293761134147644D-01,..
  5.846092323772609234D-01+%i*4.862680672667920589D-01,..
  5.958625068888068199D-01+%i*7.008561277762055397D-01,..
  5.272795078344643116D-01+%i*4.062821255065500736D-01;
  2.980741565115749836D-01+%i*6.942595774307847023D-02,..
  7.528713606297969818D-01+%i*7.671582605689764023D-01,..
  3.833705312572419643D-01+%i*1.879138792864978313D-01,..
  6.889454741030931473D-02+%i*4.096656953915953636D-01
];
Astr = assert_csvwrite(A,[],";",",");
Bstr = assert_csvread(Astr,";",",",[],%t);
B = evstr(Bstr);
MY_assert_equal ( and(B==A) , %t );
/////////////////////////////////////////////////////
//
// Read a file with an unconsistent number of columns
Atxt=[
"40 5 30 1.6 0.2 1.2"
"15 25 35 0.6 1 1.4"
"20 45 10 0.8 1.8 0.4"
""
"2.6667 0.33333 2"
"1 1.6667 2.3333"
"1.3333 3 0.66667"
];
A = assert_csvread ( Atxt," ",[],[],%t );
expected = [
"40","5","30","1.6","0.2","1.2";    
"15","25","35","0.6","1","1.4";      
"20","45","10","0.8","1.8","0.4";    
"2.6667","0.33333","2","0","0","0";  
"1","1.6667","2.3333","0","0","0";   
"1.3333","3","0.66667","0","0","0"
];
MY_assert_equal ( A , expected );
/////////////////////////////////////////////////////
//
// Read a file with an unconsistent number of columns
Atxt=[
"2.6667 0.33333 2"
"1 1.6667 2.3333"
"1.3333 3 0.66667"
""
"40 5 30 1.6 0.2 1.2"
"15 25 35 0.6 1 1.4"
"20 45 10 0.8 1.8 0.4"
];
A = assert_csvread ( Atxt," ",[],[],%t );
expected = [
"2.6667","0.33333","2","0","0","0";  
"1","1.6667","2.3333","0","0","0";   
"1.3333","3","0.66667","0","0","0"
"40","5","30","1.6","0.2","1.2";    
"15","25","35","0.6","1","1.4";      
"20","45","10","0.8","1.8","0.4";    
];
MY_assert_equal ( A , expected );
/////////////////////////////////////////////////////
//
// Read and convert into double
Astr = [
"1,8,15,22,29,36,43,50"
"2,9,16,23,30,37,44,51"
"3,10,17,6+3*I,31,38,45,52"
"4,11,18,25,32,39,46,53"
"5,12,19,26,33,40,47,54"
"6,13,20,27,34,41,48,55"
"+0,-0,Inf,-Inf,Nan,1.D+308,1.e-308,1.e-323"
];
A = assert_csvread ( Astr,[],[],[],%t,[],2 )
expected = [
1,8,15,22,29,36,43,50
2,9,16,23,30,37,44,51
3,10,17,6+3*%i,31,38,45,52
4,11,18,25,32,39,46,53
5,12,19,26,33,40,47,54
6,13,20,27,34,41,48,55
+0,-0,%inf,-%inf,%nan,1.D+308,1.e-308,1.e-323
];
MY_assert_equal ( A , expected );
/////////////////////////////////////////////////////
//
// Read only rows/columns in range from text
Astr = [
"1,8,15,22,29,36,43,50"
"2,9,16,23,30,37,44,51"
"3,10,17,6+3*I,31,38,45,52"
"4,11,18,25,32,39,46,53"
"5,12,19,26,33,40,47,54"
"6,13,20,27,34,41,48,55"
"+0,-0,Inf,-Inf,Nan,1.D+308,1.e-308,1.e-323"
];
A = assert_csvread ( Astr,[],[],[],%t,[],[],[2 3 5 6] );
expected = [
  "16","23","30","37";
  "17","6+3*%i","31","38";
  "18","25","32","39";
  "19","26","33","40"
];
MY_assert_equal ( A , expected );
/////////////////////////////////////////////////////
//
// Read only rows/columns in range from file
Astr = [
"1,8,15,22,29,36,43,50"
"2,9,16,23,30,37,44,51"
"3,10,17,6+3*I,31,38,45,52"
"4,11,18,25,32,39,46,53"
"5,12,19,26,33,40,47,54"
"6,13,20,27,34,41,48,55"
"+0,-0,Inf,-Inf,Nan,1.D+308,1.e-308,1.e-323"
];
filename=fullfile(TMPDIR,"foo.csv");
fd = mopen(filename,"w");
mputl(Astr,fd);
mclose(fd);
A = assert_csvread ( filename,[],[],[],[],[],[],[2 3 5 6] );
expected = [
  "16","23","30","37";
  "17","6+3*%i","31","38";
  "18","25","32","39";
  "19","26","33","40"
];
MY_assert_equal ( A , expected );
/////////////////////////////////////////////////////
//
// Read only rows/columns in range.
// Check with unconsistent number of columns in data.
Astr = [
"1,8,15,22,29,36,43,50"
"2,9,16,23,30"
"3,10,17,6+3*I,31,38,45,52"
"4,11,18,25,32,39,46,53"
"5,12,19"
"6,13,20,27,34,41,48,55"
"+0,-0,Inf,-Inf,Nan,1.D+308,1.e-308,1.e-323"
];
A = assert_csvread ( Astr,[],[],[],%t,[],[],[2 3 5 6] );
expected = [
    "16","23","30","0";
    "17","6+3*%i","31","38";
    "18","25","32","39";
    "19","0","0","0"
];
MY_assert_equal ( A , expected );

